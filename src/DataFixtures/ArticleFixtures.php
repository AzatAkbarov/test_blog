<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for($i = 0; $i < 30; $i++) {
            $randTag1 = rand(0,4);
            $randTag2 = rand(5,9);
            $article = new Article();
            $article
                ->setTitle($faker->sentence(3))
                ->setDescription($faker->realText(rand(500,1000)))
                ->setCreatedDate($faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'))
                ->addTag($this->getReference(TagFixtures::TAGS_NAME[$randTag1]))
                ->addTag($this->getReference(TagFixtures::TAGS_NAME[$randTag2]));
            $manager->persist($article);
        }
        $manager->flush();
    }

    function getDependencies()
    {
        return array(
            TagFixtures::class
        );
    }
}