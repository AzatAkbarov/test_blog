<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    const TAGS_NAME =
        [
            'php',
            'js',
            'java',
            'pattern',
            'html5',
            'css3',
            'git',
            'study',
            'web',
            'phpstorm'
        ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::TAGS_NAME as $key => $tag_name) {
            $tag = new Tag();
            $tag->setName($tag_name);
            $manager->persist($tag);
            $this->addReference(self::TAGS_NAME[$key], $tag);
        }
        $manager->flush();
    }
}