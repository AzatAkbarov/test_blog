<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findAllSortByDate()
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->orderBy('a.createdDate', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findAllByTagSortByDate($tag)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->innerJoin('a.tags', 't')
            ->where('t.id = :tag')
            ->setParameter('tag', $tag)
            ->orderBy('a.createdDate', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
