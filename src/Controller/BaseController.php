<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Tag;
use App\Form\ArticleType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function indexAction()
    {
        $allArticles = $this->getDoctrine()->getRepository(Article::class)->findAllSortByDate();
        return $this->render('index.html.twig', [
            'articles' => $allArticles
        ]);
    }

    public function showTags(ObjectManager $manager)
    {
        $tags = $manager->getRepository('App:Tag')->findAll();
        return $this->render('tags.html.twig', [
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/article/{id}/show", name="app_show_article_details")
     * @param $id
     * @return Response
     */
    public function showArticleDetails($id)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        return $this->render('article.html.twig', [
            'article' => $article
        ]);
    }

    /**
     * @Route("/article/add", name="app_add_article")
     * @param ObjectManager $manager
     * @param Request $request
     * @param Session $session
     * @return Response
     */
    public function addArticleAction(ObjectManager $manager, Request $request, Session $session)
    {
        $form = $this->createForm(ArticleType::class);
        $tags = $manager->getRepository('App:Tag')->findAll();
        $arrayOfNameTags = [];
        foreach ($tags as $tag) {
            $arrayOfNameTags[] = $tag->getName();
        }
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $title = trim($formData['title']);
            $description = trim($formData['description']);
            $stringOfTagNames = trim($formData['tags']);
            $stringDate = $formData['createdDate'];
            if ($title && $description && $stringOfTagNames && $stringDate) {
                try {
                    $date = date_create_from_format('j/M/Y', $formData['createdDate']);
                } catch (\Exception $e) {
                    $error = 'Выберите корректную дату!';
                    $session->getFlashBag()->set('error', $error);
                    return $this->redirectToRoute('app_add_article');
                }
                try {
                    $array_of_selected_tags = array_unique(explode(',', mb_strtolower($stringOfTagNames)));
                    $array_of_existing_tagNames = array_intersect($array_of_selected_tags, $arrayOfNameTags);
                    $array_of_new_tagsName = array_diff($array_of_selected_tags, $arrayOfNameTags);
                    $resultArrayOfNewTags = [];
                    foreach ($array_of_new_tagsName as $tagName) {
                        $tag = new Tag();
                        $tag->setName($tagName);
                        $manager->persist($tag);
                        $resultArrayOfNewTags[] = $tag;
                    }
                    $manager->flush();
                    $array_of_existing_tags = [];
                    foreach ($array_of_existing_tagNames as $existing_tagName) {
                        $tag = $this->getDoctrine()->getRepository(Tag::class)->findByTagName($existing_tagName);
                        if ($tag) {
                            $array_of_existing_tags[] = $tag;
                        }
                    }
                    $array_of_all_selectedTags = array_merge($resultArrayOfNewTags, $array_of_existing_tags);
                    $article = new Article();
                    $article->setTitle($title)
                        ->setCreatedDate($date)
                        ->setDescription($description);
                    foreach ($array_of_all_selectedTags as $selectedTag) {
                        $article->addTag($selectedTag);
                    }
                    $manager->persist($article);
                    $manager->flush();
                    return $this->redirectToRoute('homepage');
                } catch (\Exception $e) {
                    $error = 'Что то пошло не так,попробуйте еще раз';
                    $session->getFlashBag()->set('error', $error);
                    return $this->redirectToRoute('app_add_article');
                }
            } else {
                $error = 'Заполните все поля!';
                $session->getFlashBag()->set('error', $error);
                return $this->redirectToRoute('app_add_article');
            }
        }
        return $this->render('add_article.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/articles_by_tag/{id}/show", name="app_show_articles_by_tag")
     * @param $id
     * @return Response
     */
    public function showArticlesByTagNamesAction($id)
    {
        $tag = $this->getDoctrine()->getRepository(Tag::class)->find($id);
        $articles = $this->getDoctrine()->getRepository(Article::class)->findAllByTagSortByDate($tag);
        return $this->render('articles_by_tag.html.twig', [
            'articles' => $articles,
            'tagName' => $tag->getName()
        ]);
    }

    /**
     * @Route("/tags/all_tags", name="app_all_tags")
     * @return Response
     */
    public function getAllTagsAction()
    {
        $resultArray = [];
        $tags = $this->getDoctrine()->getRepository(Tag::class)->findAll();
        foreach ($tags as $tag) {
            $resultArray[] = ["text" => $tag->getName(), "value" => $tag->getName()];
        }
        return new JsonResponse($resultArray);
    }
}