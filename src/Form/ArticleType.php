<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',TextType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'Введите тему статьи'
            ]
        ])
            ->add('createdDate', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Выберите дату',
                    'readonly' => true
                ]
            ])
            ->add('tags', TextType::class, [
                'label' => false,
                'required' => true,
                'attr' => [
                    'class' => 'tagsInput',
                    'data-url' => '/tags/all_tags',
                    'data-user-option-allowed' => 'true',
                    'data-load-once' => "true",
                    'name' => 'tags',
                    'multiple' => true,
                ]
            ])
            ->add('description', TextareaType::class,[
                'label' => false,
                'attr' => [
                    'placeholder' => 'Введите текст статьи'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Добавить статью',
                'attr' => [
                    'class' => 'btn-primary btn'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_article_type';
    }

}